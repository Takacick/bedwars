package de.takacick.bedwars.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;

public class ItemSpawnListener implements Listener {

	@EventHandler
	public void onSpawn(ItemSpawnEvent event) {
		event.getEntity().setTicksLived(3600);
	}

}
