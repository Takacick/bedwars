package de.takacick.bedwars.listener;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.utils.BedWarsTeam;
import de.takacick.bedwars.utils.Gamestate;
import de.takacick.bedwars.utils.WinDetection;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class PlayerDeathListener implements Listener {

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();

		if (!BedWars.getInstance().getGamestate().equals(Gamestate.INGAME)) {
			return;
		}

		List<ItemStack> drops = new ArrayList<>();
		drops.addAll(event.getDrops());

		drops.forEach(drop -> {
			if (drop.hasItemMeta()) {
				if (drop.getItemMeta().hasItemFlag(ItemFlag.HIDE_ATTRIBUTES)) {
					event.getDrops().remove(drop);
				}
			}
		});

		if (!BedWars.getInstance().getAlive().contains(player.getUniqueId())) {
			return;
		}

		BedWarsTeam team = BedWars.getInstance().getTeam(player);
		if (team == null) {
			return;
		}

		if (!team.getBed().getBlock().getType().equals(team.getBedMaterial())) {
			if (team.getSecretBed() != null) {
				if (team.getSecretBed().getBlock().getType().equals(team.getBedMaterial())) {
					return;
				}
			}

			BedWars.getInstance().getAlive().remove(player.getUniqueId());

			team.getPlayers().forEach(uuid -> {
				if (BedWars.getInstance().getAlive().contains(uuid))
					return;
			});

			team.setDead(true);
			player.setGameMode(GameMode.SPECTATOR);

			Bukkit.getScheduler().runTaskLater(BedWars.getInstance(), () -> {
				Bukkit.broadcastMessage("The " + team.getColor() + team.getName() + " §rteam got eliminated!");

				if (WinDetection.check())
					WinDetection.getWinner();
			}, 1l);
			return;
		}

	}
}
