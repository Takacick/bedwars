package de.takacick.bedwars.listener;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.config.Config;
import de.takacick.bedwars.utils.BedWarsTeam;
import de.takacick.bedwars.utils.Gamestate;
import de.takacick.bedwars.utils.Generator;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {

	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();

		if (!event.getBlock().getType().name().contains("TERRACOTTA")) {
			if (!event.getBlock().getType().name().contains("BED")) {
				return;
			}

			BedWarsTeam team = BedWars.getInstance().getBed(event.getBlock().getLocation());
			if (team == null) {
				team = BedWars.getInstance().getSecretbed(event.getBlock().getLocation());
				if (team == null) {
					return;
				}
			}

			BedWarsTeam playerTeam = BedWars.getInstance().getTeam(player);
			if (playerTeam != null) {
				if (team.equals(playerTeam)) {
					player.sendMessage("You cannot destroy your own bed.");
					event.setCancelled(true);
					return;
				}
			}

			if (team.isBed(event.getBlock().getLocation()))
				Bukkit.broadcastMessage(
						"§rThe bed of the " + team.getColor() + team.getName() + " §rteam has been destroyed!");
			else if (team.isSecretBed(event.getBlock().getLocation()))
				Bukkit.broadcastMessage(
						"§rThe secretbed of the " + team.getColor() + team.getName() + " §rteam has been destroyed!");
		} else {
			Generator generator = BedWars.getInstance().getGenerator(event.getBlock().getLocation());
			if (generator != null) {
				if (!player.getGameMode().equals(GameMode.CREATIVE)) {
					event.setCancelled(true);
					return;
				}
			} else {
				return;
			}
			BedWars.getInstance().getGenerators().remove(generator);
			if (BedWars.getInstance().getGamestate().equals(Gamestate.BEFORE)) {
				new Config().update();
			}
		}
	}

}
