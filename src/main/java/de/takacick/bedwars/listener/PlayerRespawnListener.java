package de.takacick.bedwars.listener;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.utils.BedWarsTeam;
import de.takacick.bedwars.utils.Gamestate;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerRespawnListener implements Listener {

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		Player player = event.getPlayer();

		if (!BedWars.getInstance().getGamestate().equals(Gamestate.INGAME)) {
			return;
		}

		BedWarsTeam team = BedWars.getInstance().getTeam(player);
		if (team == null) {
			return;
		}

		if (team.isDead()) {
			player.setGameMode(GameMode.SPECTATOR);
		}

		event.setRespawnLocation(team.getSpawningPos());
		player.getInventory().clear();
		team.getArmor(player);
	}
}
