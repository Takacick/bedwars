package de.takacick.bedwars.listener;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.config.Config;
import de.takacick.bedwars.utils.Gamestate;
import de.takacick.bedwars.utils.Generator;
import de.takacick.bedwars.utils.GeneratorType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceListener implements Listener {

	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		if (!event.getBlock().getType().name().contains("TERRACOTTA")) {
			return;
		}

		GeneratorType type = BedWars.getInstance().getGeneratorType(event.getBlock().getType());
		if (type == null) {
			return;
		}

		Generator generator = new Generator(type, event.getBlock().getLocation());
		BedWars.getInstance().getGenerators().add(generator);
		if (BedWars.getInstance().getGamestate().equals(Gamestate.BEFORE)) {
			new Config().update();
		}
	}
}
