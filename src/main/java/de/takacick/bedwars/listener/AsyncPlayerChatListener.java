package de.takacick.bedwars.listener;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.utils.BedWarsTeam;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener implements Listener {

	@EventHandler(priority = EventPriority.LOW)
	public void onAsync(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();

		BedWarsTeam team = BedWars.getInstance().getTeam(player);
		if (team == null) {
			return;
		}

		event.setFormat(event.getFormat().replace("%1$s", team.getColor() + "%1$s§r"));
	}
}
