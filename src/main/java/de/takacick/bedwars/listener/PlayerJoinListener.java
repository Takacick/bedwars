package de.takacick.bedwars.listener;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.utils.BedWarsTeam;
import de.takacick.bedwars.utils.Gamestate;
import de.takacick.bedwars.utils.ScoreboardHandler;
import de.takacick.bedwars.utils.WinDetection;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerJoinListener implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();

		if (!BedWars.getInstance().getGamestate().equals(Gamestate.INGAME)) {
			Bukkit.getScheduler().runTaskLater(BedWars.getInstance(), () -> {
				player.setGameMode(GameMode.SPECTATOR);
				player.getInventory().clear();
				player.setExp(0);
				player.setHealth(player.getMaxHealth());
				player.setFoodLevel(20);
			}, 1l);
		} else {

			if (!BedWars.getInstance().getAlive().contains(player.getUniqueId())) {
				BedWarsTeam team = BedWars.getInstance().getTeam(player);
				if (team != null) {
					if (team.getBed() != null) {
						if (!team.getBed().getBlock().getType().equals(team.getBedMaterial())) {
							if (team.getSecretBed() != null) {
								if (team.getSecretBed().getBlock().getType().equals(team.getBedMaterial())) {
									BedWars.getInstance().getAlive().add(player.getUniqueId());
								}
							}
						} else {
							BedWars.getInstance().getAlive().add(player.getUniqueId());
						}
					}
				}
			}
		}

		ScoreboardHandler.setScoreboard(player);
		ScoreboardHandler.update(player, BedWars.getInstance().getTeam(player));
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();

		BedWarsTeam team = BedWars.getInstance().getTeam(player);
		if (team != null) {
			BedWars.getInstance().getAlive().remove(player.getUniqueId());
			if (WinDetection.check())
				WinDetection.getWinner();
		}
	}
}
