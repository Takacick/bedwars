package de.takacick.bedwars.commands;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.config.Config;
import de.takacick.bedwars.utils.BedWarsTeam;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class SecretBedCommand implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return true;
		}

		if (!sender.hasPermission("BedWars.SecretBed")) {
			sender.sendMessage("§cYou are not allowed to perform this command!");
			return true;
		}

		if (args.length != 1) {
			sender.sendMessage("Use /secretbed <colour>");
			sender.sendMessage("Use /secretbed on/off");
			return true;
		}
		final Player player = (Player) sender;

		if (args[0].equalsIgnoreCase("on")) {
			BedWars.getInstance().setSecretbed(true);

			for (BedWarsTeam team : BedWars.getInstance().getTeams()) {
				if (team.getSecretBed() != null)
					team.setBed(team.getSecretBed().getBlock(), team.getSecretFace(), true);
			}
			player.sendMessage("Secretbeds are now enabled!");

			return true;
		} else if (args[0].equalsIgnoreCase("off")) {
			BedWars.getInstance().setSecretbed(false);

			for (BedWarsTeam team : BedWars.getInstance().getTeams()) {
				if (team.getSecretBed() != null)
					team.destroyBed(team.getSecretBed().getBlock(), team.getSecretFace(), true, false);
			}
			player.sendMessage("Secretbeds are now disabled!");

			return true;
		}

		BedWarsTeam team = BedWars.getInstance().getTeam(args[0]);
		if (team == null) {
			player.sendMessage("The team " + args[0] + " doesn't exists!");
			return true;
		}

		if (team.getSecretBed() != null)
			team.destroyBed(team.getSecretBed().getBlock(), team.getSecretFace(), true, true);

		player.sendMessage("You set the secretbed for the " + team.getColor() + team.getName() + " §rteam.");
		team.setBed(player.getLocation().getBlock(), player.getFacing().getOppositeFace(), true);
		new Config().update();

		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return null;
		}

		if (!sender.hasPermission("BedWars.SecretBed")) {
			return null;
		}

		if (args.length == 1) {
			List<String> help = new ArrayList<>();

			String searching = "";

			if (args[0] != null) {
				searching = args[0].toLowerCase();
			}

			if ("off".startsWith(searching)) {
				help.add("off");
			}

			if ("on".startsWith(searching)) {
				help.add("on");
			}

			for (BedWarsTeam team : BedWars.getInstance().getTeams()) {
				if (team.getName().toLowerCase().startsWith(searching)) {
					help.add(team.getName());
				}
			}

			return help;
		}

		return null;
	}
}
