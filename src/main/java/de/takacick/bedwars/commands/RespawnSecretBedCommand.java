package de.takacick.bedwars.commands;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.utils.BedWarsTeam;
import de.takacick.bedwars.utils.Gamestate;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class RespawnSecretBedCommand implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!sender.hasPermission("BedWars.RespawnSecretBed")) {
			sender.sendMessage("§cYou are not allowed to perform this command!");
			return true;
		}

		if (args.length != 1) {
			sender.sendMessage("Use /respawnsecretbed <colour>");
			return true;
		}

		if (!BedWars.getInstance().getGamestate().equals(Gamestate.INGAME)) {
			sender.sendMessage("You cannot respawn an secretbed at this gamestate!");
			return true;
		}

		final Player player = (Player) sender;

		if (!BedWars.getInstance().getSecretBed()) {
			player.sendMessage("Secretbeds are disabled!");
			return true;
		}

		BedWarsTeam team = BedWars.getInstance().getTeam(args[0]);

		if (team == null) {
			player.sendMessage("The team " + args[0] + " doesn't exists!");
			return true;
		}

		if (team.getSecretBed() == null) {
			player.sendMessage("The " + team.getColor() + team.getName() + " §rteam doesn't have an secretbed!");
			return true;
		}

		if (team.getSecretBed().getBlock().getType().equals(team.getBedMaterial())) {
			player.sendMessage("The secretbed of the " + team.getColor() + team.getName() + " §rteam isn't destroyed!");
			return true;
		}

		player.sendMessage("You respawned the secretbed for the " + team.getColor() + team.getName() + " §rteam.");
		team.setBed(team.getSecretBed().getBlock(), team.getSecretFace(), true);
		
		team.setDead(false);
		
		team.getPlayers().forEach(uuid -> {
			Player target = Bukkit.getPlayer(uuid);
			if (!BedWars.getInstance().getAlive().contains(uuid)) {
				if (target != null) {
					target.teleport(team.getSpawningPos());
					target.setGameMode(GameMode.SURVIVAL);
				}
			}
		});
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return null;
		}

		if (!sender.hasPermission("BedWars.RespawnSecretBed")) {
			return null;
		}

		if (args.length == 1) {
			List<String> help = new ArrayList<>();

			String searching = "";

			if (args[0] != null) {
				searching = args[0].toLowerCase();
			}

			for (BedWarsTeam team : BedWars.getInstance().getTeams()) {
				if (team.getName().toLowerCase().startsWith(searching)) {
					help.add(team.getName());
				}
			}

			return help;
		}

		return null;
	}

}
