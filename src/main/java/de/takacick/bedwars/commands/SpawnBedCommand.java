package de.takacick.bedwars.commands;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.config.Config;
import de.takacick.bedwars.utils.BedWarsTeam;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class SpawnBedCommand implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return true;
		}

		final Player player = (Player) sender;

		if (!player.hasPermission("BedWars.SpawnBed")) {
			player.sendMessage("§cYou are not allowed to perform this command!");
			return true;
		}

		if (args.length != 1) {
			player.sendMessage("Use /spawnbed <colour>");
			return true;
		}

		BedWarsTeam team = BedWars.getInstance().getTeam(args[0]);
		if (team == null) {
			player.sendMessage("The team " + args[0] + " doesn't exists!");
			return true;
		}

		if (team.getBed() != null)
			team.destroyBed(team.getBed().getBlock(), team.getBedFace(), false, true);

		player.sendMessage("You set the bed for the " + team.getColor() + team.getName() + " §rteam.");
		team.setBed(player.getLocation().getBlock(), player.getFacing().getOppositeFace(), false);
		new Config().update();

		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return null;
		}

		if (!sender.hasPermission("BedWars.SpawnBed")) {
			return null;
		}

		if (args.length == 1) {
			List<String> help = new ArrayList<>();

			String searching = "";

			if (args[0] != null) {
				searching = args[0].toLowerCase();
			}

			for (BedWarsTeam team : BedWars.getInstance().getTeams()) {
				if (team.getName().toLowerCase().startsWith(searching)) {
					help.add(team.getName());
				}
			}

			return help;
		}

		return null;
	}

}
