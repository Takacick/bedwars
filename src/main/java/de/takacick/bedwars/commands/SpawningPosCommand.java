package de.takacick.bedwars.commands;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.config.Config;
import de.takacick.bedwars.utils.BedWarsTeam;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class SpawningPosCommand implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return true;
		}

		Player player = (Player) sender;

		if (!player.hasPermission("BedWars.SpawningPos")) {
			player.sendMessage("§cYou are not allowed to perform this command!");
			return true;
		}

		if (args.length != 1) {
			player.sendMessage("Use /spawningpos <colour>");
			return true;
		}

		BedWarsTeam team = BedWars.getInstance().getTeam(args[0]);
		if (team == null) {
			player.sendMessage("The team " + args[0] + " doesn't exists!");
			return true;
		}

		team.setSpawningPos(player.getLocation());
		player.sendMessage("You set the spawnpoint for the " + team.getColor() + team.getName() + " §rteam.");
		new Config().update();

		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return null;
		}

		if (!sender.hasPermission("BedWars.SpawningPos")) {
			return null;
		}

		if (args.length == 1) {
			List<String> help = new ArrayList<>();

			String searching = "";

			if (args[0] != null) {
				searching = args[0].toLowerCase();
			}

			for (BedWarsTeam team : BedWars.getInstance().getTeams()) {
				if (team.getName().toLowerCase().startsWith(searching)) {
					help.add(team.getName());
				}
			}

			return help;
		}

		return null;
	}

}
