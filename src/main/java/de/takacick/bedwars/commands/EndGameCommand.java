package de.takacick.bedwars.commands;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.countdown.RestartCountdown;
import de.takacick.bedwars.utils.Gamestate;
import de.takacick.bedwars.utils.WinDetection;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class EndGameCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!sender.hasPermission("BedWars.EndGame")) {
			sender.sendMessage("§cYou are not allowed to perform this command!");
			return true;
		}

		if (BedWars.getInstance().getGamestate().equals(Gamestate.BEFORE)) {
			sender.sendMessage("§cYou cannot end the game yet!");
			return true;
		}

		if(WinDetection.check())
			WinDetection.getWinner();
		else {
			if (BedWars.getInstance().getRestartCountdown() == null)
				BedWars.getInstance().setRestartCountdown(new RestartCountdown(false)).start();
			BedWars.getInstance().setGamestate(Gamestate.AFTER);
			return true;
		}
		return false;
	}

}
