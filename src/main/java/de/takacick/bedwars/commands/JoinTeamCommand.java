package de.takacick.bedwars.commands;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.utils.BedWarsTeam;
import de.takacick.bedwars.utils.Gamestate;
import de.takacick.bedwars.utils.ScoreboardHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class JoinTeamCommand implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return true;
		}

		Player player = (Player) sender;

		if (!BedWars.getInstance().getGamestate().equals(Gamestate.BEFORE)) {
			sender.sendMessage("§cThe round already started!");
			return true;
		}

		if (args.length == 0) {
			player.sendMessage("Use /jointeam <colour>");
			return true;
		}

		BedWarsTeam team = BedWars.getInstance().getTeam(args[0]);
		if (team == null) {
			player.sendMessage("The team " + args[0] + " doesn't exists!");
			return true;
		}

		BedWarsTeam old = BedWars.getInstance().getTeam(player);
		if (old != null) {
			old.getPlayers().remove(player.getUniqueId());
		}

		team.getPlayers().add(player.getUniqueId());
		player.sendMessage("You joined the " + team.getColor() + team.getName() + " §rteam.");

		ScoreboardHandler.update(player, team);
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return null;
		}

		if (args.length == 1) {
			List<String> help = new ArrayList<>();

			String searching = "";

			if (args[0] != null) {
				searching = args[0].toLowerCase();
			}

			for (BedWarsTeam team : BedWars.getInstance().getTeams()) {
				if (team.getName().toLowerCase().startsWith(searching)) {
					help.add(team.getName());
				}
			}

			return help;
		}

		return null;
	}

}
