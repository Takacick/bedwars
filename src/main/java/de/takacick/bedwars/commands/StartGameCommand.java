package de.takacick.bedwars.commands;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.utils.BedWarsTeam;
import de.takacick.bedwars.utils.Gamestate;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class StartGameCommand implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!sender.hasPermission("BedWars.Start")) {
			sender.sendMessage("§cYou are not allowed to perform this command!");
			return true;
		}

		if (!BedWars.getInstance().getGamestate().equals(Gamestate.BEFORE)) {
			sender.sendMessage("§cThe round already started!");
			return true;
		}

		sender.sendMessage("§rchecking setup..");

		for (BedWarsTeam team : BedWars.getInstance().getTeams()) {
			if (!team.getPlayers().isEmpty()) {
				if (team.getSpawningPos() == null) {
					sender.sendMessage("The " + team.getColor() + team.getName() + " §rteam has no spawnpoint!");
					return true;
				} else if (team.getBed() == null) {
					sender.sendMessage("The " + team.getColor() + team.getName() + " §rteam has no bed!");
					return true;
				}
			}
		}

		sender.sendMessage("§rteleporting all players..");

		for (BedWarsTeam team : BedWars.getInstance().getTeams()) {
			if (team.getPlayers().isEmpty()) {
				if (team.getBed() != null)
					team.destroyBed(team.getBed().getBlock(), team.getBedFace(), false, false);
				if (team.getSecretBed() != null)
					team.destroyBed(team.getSecretBed().getBlock(), team.getSecretFace(), true, false);
			} else {
				team.setBed(team.getBed().getBlock(), team.getBedFace(), false);
				if (team.getSecretBed() != null)
					team.setBed(team.getSecretBed().getBlock(), team.getSecretFace(), true);

				team.getPlayers().forEach(uuid -> {
					Player player = Bukkit.getPlayer(uuid);
					if (player != null) {
						player.getInventory().clear();
						player.setExp(0);
						player.setHealth(player.getMaxHealth());
						player.setFoodLevel(20);
						player.teleport(team.getSpawningPos());
						player.setGameMode(GameMode.SURVIVAL);
						BedWars.getInstance().getAlive().add(player.getUniqueId());
						team.getArmor(player);
					}
				});
			}
		}

		Bukkit.getWorlds().forEach(world -> {
			world.setAutoSave(false);
			copyFileStructure(world.getWorldFolder(),
					new File(world.getWorldFolder().getName().replace(world.getName(), world.getName() + "_backup")));
		});

		sender.sendMessage("§rgame has been started! Have fun!");

		BedWars.getInstance().setGamestate(Gamestate.INGAME);

		return false;
	}

	private static void copyFileStructure(File source, File target) {
		try {
			ArrayList<String> ignore = new ArrayList<>(Arrays.asList("uid.dat", "session.lock"));
			if (!ignore.contains(source.getName())) {
				if (source.isDirectory()) {
					if (!target.exists())
						if (!target.mkdirs())
							throw new IOException("Couldn't create world directory!");
					String files[] = source.list();
					for (String file : files) {
						File srcFile = new File(source, file);
						File destFile = new File(target, file);
						copyFileStructure(srcFile, destFile);
					}
				} else {
					InputStream in = new FileInputStream(source);
					OutputStream out = new FileOutputStream(target);
					byte[] buffer = new byte[1024];
					int length;
					while ((length = in.read(buffer)) > 0)
						out.write(buffer, 0, length);
					in.close();
					out.close();
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean unloadWorld(World world) {
		return world != null && Bukkit.getServer().unloadWorld(world, false);
	}

	public static void copyWorld(World originalWorld, String newWorldName) {
		copyFileStructure(originalWorld.getWorldFolder(), new File(Bukkit.getWorldContainer(), newWorldName));
	}

}
