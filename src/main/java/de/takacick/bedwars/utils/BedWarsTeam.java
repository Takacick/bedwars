package de.takacick.bedwars.utils;

import de.takacick.bedwars.api.ItemBuilder;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.Bed;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BedWarsTeam {

	private String name;
	private ChatColor color;
	private Color armorColor;
	private List<UUID> players;
	private Location bed;
	private Material bedmaterial;
	private Location secret;
	private Location spawningPos;
	private BlockFace bedFace;
	private BlockFace secretbedFace;
	private boolean dead;

	public BedWarsTeam(String name, ChatColor color, Color armorColor, Material bed) {
		this.name = name;
		this.color = color;
		this.armorColor = armorColor;
		this.players = new ArrayList<>();
		this.bedmaterial = bed;
		this.dead = false;
	}

	public String getName() {
		return name;
	}

	public ChatColor getColor() {
		return color;
	}

	public List<UUID> getPlayers() {
		return players;
	}

	public Location getBed() {
		return bed;
	}

	public Material getBedMaterial() {
		return bedmaterial;
	}

	public BedWarsTeam setBed(Location bed) {
		this.bed = bed;
		return this;
	}

	public Location getSpawningPos() {
		return spawningPos;
	}

	public void setSpawningPos(Location location) {
		this.spawningPos = location;
	}

	public BedWarsTeam setSecretBed(Location location) {
		this.secret = location;
		return this;
	}

	public Location getSecretBed() {
		return secret;
	}

	public BlockFace getSecretFace() {
		return secretbedFace;
	}

	public BlockFace getBedFace() {
		return bedFace;
	}

	public boolean isDead() {
		return dead;
	}

	public BedWarsTeam setDead(boolean dead) {
		this.dead = dead;
		return this;
	}

	public void getArmor(Player player) {
		player.getInventory()
				.setHelmet(new ItemBuilder(Material.LEATHER_HELMET).setLeatherArmorColor(armorColor).toItemStack());
		player.getInventory().setChestplate(
				new ItemBuilder(Material.LEATHER_CHESTPLATE).setLeatherArmorColor(armorColor).toItemStack());
		player.getInventory()
				.setLeggings(new ItemBuilder(Material.LEATHER_LEGGINGS).setLeatherArmorColor(armorColor).toItemStack());
		player.getInventory()
				.setBoots(new ItemBuilder(Material.LEATHER_BOOTS).setLeatherArmorColor(armorColor).toItemStack());
		player.getInventory().setItem(0, new ItemBuilder(new ItemStack(Material.WOODEN_SWORD)).toItemStack());
	}

	public boolean setBed(Block start, BlockFace facing, boolean secret) {

		if (start == null) {
			return false;
		}

		if (secret) {
			this.secret = start.getLocation();
			this.secretbedFace = facing;
		} else {
			this.bed = start.getLocation();
			this.bedFace = facing;
		}
		for (Bed.Part part : Bed.Part.values()) {
			start.setBlockData(Bukkit.createBlockData(bedmaterial, (data) -> {
				((Bed) data).setPart(part);
				((Bed) data).setFacing(facing);
			}));
			start = start.getRelative(facing.getOppositeFace());
		}

		return true;
	}

	public boolean destroyBed(Block start, BlockFace facing, boolean secret, boolean delete) {

		if (start == null) {
			return false;
		}

		if (delete) {
			if (secret)
				this.secret = null;
			else
				this.bed = null;
		}
		start.getRelative(facing.getOppositeFace()).setType(Material.AIR, false);
		start.setType(Material.AIR, false);

		return true;
	}

	public boolean isBed(Location location) {
		if (bed == null)
			return false;

		if (bed.getBlock().getRelative(bedFace.getOppositeFace()).getLocation().equals(location))
			return true;

		if (bed.equals(location))
			return true;

		return false;
	}

	public boolean isSecretBed(Location location) {
		if (secret == null)
			return false;

		if (secret.getBlock().getRelative(secretbedFace.getOppositeFace()).getLocation().equals(location))
			return true;

		if (secret.equals(location))
			return true;

		return false;
	}
}
