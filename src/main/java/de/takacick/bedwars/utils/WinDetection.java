package de.takacick.bedwars.utils;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.countdown.RestartCountdown;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class WinDetection {

	public static boolean check() {
		List<BedWarsTeam> teams = new ArrayList<>();

		BedWars.getInstance().getTeams().forEach(team -> {
			team.getPlayers().forEach(uuid -> {
				if (!teams.contains(team)) {
					if (BedWars.getInstance().getAlive().contains(uuid)) {
						teams.add(team);
					}
				}
			});
		});

		if (teams.size() <= 1) {
			return true;
		}

		return false;
	}

	public static void getWinner() {
		if (BedWars.getInstance().getAlive().size() <= 0) {
			Bukkit.broadcastMessage("no winner found!");
			if (BedWars.getInstance().getRestartCountdown() == null)
				BedWars.getInstance().setRestartCountdown(new RestartCountdown(false)).start();
			BedWars.getInstance().setGamestate(Gamestate.AFTER);
			return;
		}

		if (BedWars.getInstance().getAlive().size() == 1 || check()) {
			Player player = Bukkit.getPlayer(BedWars.getInstance().getAlive().get(0));
			if (player == null) {
				Bukkit.broadcastMessage("no winner found!");
				if (BedWars.getInstance().getRestartCountdown() == null)
					BedWars.getInstance().setRestartCountdown(new RestartCountdown(false)).start();
				BedWars.getInstance().setGamestate(Gamestate.AFTER);
				return;
			}

			BedWarsTeam team = BedWars.getInstance().getTeam(player);
			if (team == null) {
				Bukkit.broadcastMessage("no winner found!");
				if (BedWars.getInstance().getRestartCountdown() == null)
					BedWars.getInstance().setRestartCountdown(new RestartCountdown(false)).start();
				return;
			}

			Bukkit.broadcastMessage("The " + team.getColor() + team.getName() + " §rteam has won the round!");
			if (BedWars.getInstance().getRestartCountdown() == null)
				BedWars.getInstance().setRestartCountdown(new RestartCountdown(false)).start();
			BedWars.getInstance().setGamestate(Gamestate.AFTER);
			return;
		}
	}
}
