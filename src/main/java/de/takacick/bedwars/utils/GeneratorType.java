package de.takacick.bedwars.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.loot.LootContext;
import org.bukkit.loot.LootTable;

import java.util.List;

public class GeneratorType {

	private Material material;
	private int min;
	private int max;
	private List<ItemStack> drops;
	private LootTable table;
	private LootContext context;
	private boolean random;

	public GeneratorType(Material material, int min, int max, List<ItemStack> drops, boolean random) {
		this.material = material;
		this.min = min;
		this.max = max;
		this.drops = drops;
		this.random = random;
	}

	public GeneratorType(Material material, int min, int max, LootTable table, boolean random) {
		this.material = material;
		this.min = min;
		this.max = max;
		this.table = table;
		this.random = random;
	}

	public Material getMaterial() {
		return material;
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}

	public List<ItemStack> getDrops() {
		return drops;
	}

	public LootTable getLootTable() {
		return table;
	}
	
	public LootContext getContext() {
		return context;
	}

	public boolean isRandom() {
		return random;
	}
}
