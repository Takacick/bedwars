package de.takacick.bedwars.utils;

import de.takacick.bedwars.BedWars;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.Hopper;
import org.bukkit.inventory.ItemStack;
import org.bukkit.loot.Lootable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {

	private GeneratorType generator;
	private Location block;
	private Location drop;

	public Generator(GeneratorType generator, Location block) {
		this.generator = generator;
		this.block = block;
		this.drop = block.clone().add(0.5, 1.2, 0.5);
		if (generator.getLootTable() == null)
			drop();
		else
			dropLootTable();
	}

	private void drop() {
		Bukkit.getScheduler().runTaskLater(BedWars.getInstance(), () -> {
			if (!BedWars.getInstance().getGamestate().equals(Gamestate.INGAME)) {
				drop();
				return;
			}
			if (!BedWars.getInstance().getGenerators().contains(this)) {
				return;
			}

			if (generator.isRandom()) {
				drop.getWorld().dropItem(drop,
						generator.getDrops().get(new Random().nextInt(generator.getDrops().size())));
			} else {
				generator.getDrops().forEach(item -> {
					drop.getWorld().dropItem(drop, item);
				});
			}
			drop();
		}, (new Random().nextInt(generator.getMax() - generator.getMin()) + generator.getMin() + 1) * 20l);
	}

	private void dropLootTable() {
		Bukkit.getScheduler().runTaskLater(BedWars.getInstance(), () -> {
			if (!BedWars.getInstance().getGamestate().equals(Gamestate.INGAME)) {
				dropLootTable();
				return;
			}
			
			if (!BedWars.getInstance().getGenerators().contains(this)) {
				return;
			}

			List<ItemStack> items = new ArrayList<>();
			Location under = block.clone();
			under.setY(1);

			Material before = under.getBlock().getType();
			under.getBlock().setType(Material.CHEST);

			Location under2 = block.clone();
			under2.setY(0);

			BlockState state = under.getBlock().getState();
			Lootable lootable = (Lootable) state; // Note that some Containers are not Lootable.

			lootable.setLootTable(generator.getLootTable()); // Assign your LootTable.
			state.update(true);

			Material before2 = under2.getBlock().getType();
			under2.getBlock().setType(Material.HOPPER);
			Bukkit.getScheduler().runTaskLater(BedWars.getInstance(), () -> {
				Hopper hopper = (Hopper) under2.getBlock().getState();
				if (hopper.getInventory().getItem(0) != null) {
					items.add(hopper.getInventory().getItem(0));
					hopper.getInventory().clear();
				}
				under2.getBlock().setType(before2);
			}, 1l);

			Bukkit.getScheduler().runTaskLater(BedWars.getInstance(), () -> {
				Chest chest = (Chest) state;

				for (ItemStack itemstack : chest.getInventory().getContents()) {
					if (itemstack != null) {
						items.add(itemstack);
					}
				}

				chest.getInventory().clear();

				if (items.size() == 0) {
					dropLootTable();
					return;
				}

				if (generator.isRandom()) {
					drop.getWorld().dropItem(drop, items.get(new Random().nextInt(items.size())));
				} else {
					items.forEach(item -> {
						drop.getWorld().dropItem(drop, item);
					});
				}
				under.getBlock().setType(Material.AIR);
				under.getBlock().setType(before);
				dropLootTable();
			}, 1l);
		}, (new Random().nextInt(generator.getMax() - generator.getMin()) + generator.getMin() + 1) * 20l);
	}

	public Location getBlock() {
		return block;
	}
}
