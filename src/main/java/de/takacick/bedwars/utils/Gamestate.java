package de.takacick.bedwars.utils;

public enum Gamestate {
	
	BEFORE,
	INGAME,
	AFTER;
	
}
