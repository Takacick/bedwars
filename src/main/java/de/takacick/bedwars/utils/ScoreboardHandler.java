package de.takacick.bedwars.utils;

import de.takacick.bedwars.BedWars;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class ScoreboardHandler {

	public static void setScoreboard(Player player) {
		final Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		BedWars.getInstance().getTeams().forEach(team -> {
			Team scoreTeam = board.registerNewTeam(team.getName());
			scoreTeam.setColor(team.getColor());
			team.getPlayers().forEach(uuid -> {
				Player teamPlayer = Bukkit.getPlayer(uuid);
				if (teamPlayer != null) {
					if (!scoreTeam.hasEntry(teamPlayer.getName()))
						scoreTeam.addEntry(teamPlayer.getName());
				}
			});
		});

		player.setScoreboard(board);
	}

	public static void update(Player player, BedWarsTeam team) {

		Bukkit.getOnlinePlayers().forEach(online -> {
			final Scoreboard board = online.getScoreboard();
			if (board == null) {
				setScoreboard(online);
			} else {
				if (team == null) {
					if (board.getEntryTeam(player.getName()) != null) {
						board.getEntryTeam(player.getName()).removeEntry(player.getName());
					}
				} else {
					if (board.getTeam(team.getName()) == null) {
						board.registerNewTeam(team.getName()).setColor(team.getColor());
						board.getTeam(team.getName()).setPrefix("§b" + "");
						board.getTeam(team.getName()).setSuffix("§r");
					}

					Team scoreTeam = board.getTeam(team.getName());

					if (!scoreTeam.hasEntry(player.getName()))
						scoreTeam.addEntry(player.getName());
				}
			}
		});
	}
}
