package de.takacick.bedwars;

import de.takacick.bedwars.commands.*;
import de.takacick.bedwars.config.Config;
import de.takacick.bedwars.countdown.Countdown;
import de.takacick.bedwars.listener.*;
import de.takacick.bedwars.utils.BedWarsTeam;
import de.takacick.bedwars.utils.Gamestate;
import de.takacick.bedwars.utils.Generator;
import de.takacick.bedwars.utils.GeneratorType;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.loot.LootTables;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class BedWars extends JavaPlugin {

	private static BedWars instance;
	private List<BedWarsTeam> teams;
	private Gamestate state;
	private boolean secretbed;
	private List<UUID> alive;
	private List<GeneratorType> types;
	private List<Generator> generator;
	private Countdown restartCountdown;

	@Override
	public void onLoad() {
		if (Bukkit.getWorldContainer().getAbsoluteFile().exists()) {
			File[] files = Bukkit.getWorldContainer().getAbsoluteFile().listFiles();

			for (File file : files) {
				if (file.isDirectory()) {
					if (file.getName().contains("_backup")) {
						String world = file.getName().replace("_backup", "");
							new File(Bukkit.getWorldContainer() + "/" + world).delete();

						copyFileStructure(file, new File(Bukkit.getWorldContainer() + "/" + world));

						file.delete();
					}
				}
			}
		}
	}

	@Override
	public void onEnable() {
		instance = this;
		state = Gamestate.BEFORE;
		secretbed = false;

		teams = new ArrayList<>();
		alive = new ArrayList<>();
		types = new ArrayList<>();
		generator = new ArrayList<>();

		Bukkit.getPluginCommand("endgame").setExecutor(new EndGameCommand());
		Bukkit.getPluginCommand("jointeam").setExecutor(new JoinTeamCommand());
		Bukkit.getPluginCommand("respawnbed").setExecutor(new RespawnBedCommand());
		Bukkit.getPluginCommand("respawnsecretbed").setExecutor(new RespawnSecretBedCommand());
		Bukkit.getPluginCommand("secretbed").setExecutor(new SecretBedCommand());
		Bukkit.getPluginCommand("spawnbed").setExecutor(new SpawnBedCommand());
		Bukkit.getPluginCommand("spawningpos").setExecutor(new SpawningPosCommand());
		Bukkit.getPluginCommand("startgame").setExecutor(new StartGameCommand());

		Bukkit.getPluginManager().registerEvents(new AsyncPlayerChatListener(), this);
		Bukkit.getPluginManager().registerEvents(new BlockBreakListener(), this);
		Bukkit.getPluginManager().registerEvents(new BlockPlaceListener(), this);
		Bukkit.getPluginManager().registerEvents(new ItemSpawnListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerDeathListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerRespawnListener(), this);
		Bukkit.getPluginManager().registerEvents(new CreatureSpawnListener(), this);

		teams.add(new BedWarsTeam("Red", ChatColor.RED, Color.RED, Material.RED_BED));
		teams.add(new BedWarsTeam("Blue", ChatColor.BLUE, Color.BLUE, Material.BLUE_BED));
		teams.add(new BedWarsTeam("Yellow", ChatColor.YELLOW, Color.YELLOW, Material.YELLOW_BED));
		teams.add(new BedWarsTeam("Green", ChatColor.GREEN, Color.GREEN, Material.LIME_BED));
		teams.add(new BedWarsTeam("Purple", ChatColor.DARK_PURPLE, Color.PURPLE, Material.PURPLE_BED));
		teams.add(new BedWarsTeam("Pink", ChatColor.LIGHT_PURPLE, Color.FUCHSIA, Material.PINK_BED));
		teams.add(new BedWarsTeam("Orange", ChatColor.GOLD, Color.ORANGE, Material.ORANGE_BED));
		teams.add(new BedWarsTeam("White", ChatColor.WHITE, Color.WHITE, Material.WHITE_BED));
		teams.add(new BedWarsTeam("Black", ChatColor.BLACK, Color.BLACK, Material.BLACK_BED));

		types.add(new GeneratorType(Material.YELLOW_GLAZED_TERRACOTTA, 2, 10,
				Arrays.asList(new ItemStack(Material.GOLD_INGOT, 1)), false));
		types.add(new GeneratorType(Material.BLUE_GLAZED_TERRACOTTA, 10, 25,
				Arrays.asList(new ItemStack(Material.DIAMOND, 1)), false));
		types.add(new GeneratorType(Material.WHITE_GLAZED_TERRACOTTA, 2, 10,
				Arrays.asList(new ItemStack(Material.IRON_INGOT, 1)), false));
		types.add(new GeneratorType(Material.LIME_GLAZED_TERRACOTTA, 15, 25,
				Arrays.asList(new ItemStack(Material.EMERALD, 1)), false));
		types.add(new GeneratorType(Material.RED_GLAZED_TERRACOTTA, 10, 30,
				Arrays.asList(new ItemStack(Material.TNT, 1)), false));
		types.add(new GeneratorType(Material.CYAN_GLAZED_TERRACOTTA, 10, 20,
				Arrays.asList(new ItemStack(Material.OBSIDIAN, 1)), false));
		types.add(new GeneratorType(Material.BROWN_GLAZED_TERRACOTTA, 20, 45,
				Arrays.asList(new ItemStack(Material.TOTEM_OF_UNDYING, 1)), false));
		types.add(new GeneratorType(Material.GREEN_GLAZED_TERRACOTTA, 10, 30,
				Arrays.asList(new ItemStack(Material.CREEPER_SPAWN_EGG, 1)), false));
		types.add(new GeneratorType(Material.PINK_GLAZED_TERRACOTTA, 15, 30,
				LootTables.END_CITY_TREASURE.getLootTable(), true));
		types.add(new GeneratorType(Material.PURPLE_GLAZED_TERRACOTTA, 3, 10,
				Arrays.asList(new ItemStack(Material.OAK_LOG, 1)), false));

		new Config().load();
		// sending a enabled message with plugin name and version number
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getLogger().info(pdfFile.getName() + " Version: " + pdfFile.getVersion() + " is now enabled!");
	}

	@Override
	public void onDisable() {

		// sending a disable message with plugin name and version number
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getLogger().info(pdfFile.getName() + " Version: " + pdfFile.getVersion() + " is now disabled!");
	}

	public static BedWars getInstance() {
		return instance;
	}

	public List<BedWarsTeam> getTeams() {
		return teams;
	}

	public List<UUID> getAlive() {
		return alive;
	}

	public List<GeneratorType> getGeneratorTypes() {
		return types;
	}

	public List<Generator> getGenerators() {
		return generator;
	}

	public Gamestate getGamestate() {
		return state;
	}

	public Countdown getRestartCountdown() {
		return restartCountdown;
	}

	public Countdown setRestartCountdown(Countdown countdown) {
		this.restartCountdown = countdown;

		return countdown;
	}

	public boolean getSecretBed() {
		return secretbed;
	}

	public void setSecretbed(boolean secretbed) {
		this.secretbed = secretbed;
	}

	public void setGamestate(Gamestate state) {
		this.state = state;
	}

	public BedWarsTeam getTeam(String team) {
		for (BedWarsTeam bedteam : teams) {
			if (bedteam.getName().equalsIgnoreCase(team)) {
				return bedteam;
			}
		}

		return null;
	}

	public BedWarsTeam getTeam(Player player) {
		for (BedWarsTeam bedteam : teams) {
			if (bedteam.getPlayers().contains(player.getUniqueId())) {
				return bedteam;
			}
		}

		return null;
	}

	public BedWarsTeam getBed(Location bed) {
		for (BedWarsTeam bedteam : teams) {
			if (bedteam.isBed(bed)) {
				return bedteam;
			}
		}

		return null;
	}

	public BedWarsTeam getSecretbed(Location bed) {
		for (BedWarsTeam bedteam : teams) {
			if (bedteam.isSecretBed(bed)) {
				return bedteam;
			}
		}

		return null;
	}

	public GeneratorType getGeneratorType(Material material) {
		for (GeneratorType generator : types) {
			if (generator.getMaterial().equals(material)) {
				return generator;
			}
		}

		return null;
	}

	public Generator getGenerator(Location block) {
		for (Generator generator : this.generator) {
			if (generator.getBlock().equals(block)) {
				return generator;
			}
		}

		return null;
	}

	private static void copyFileStructure(File source, File target) {
		try {
			ArrayList<String> ignore = new ArrayList<>(Arrays.asList("uid.dat", "session.lock"));
			if (!ignore.contains(source.getName())) {
				if (source.isDirectory()) {
					if (!target.exists())
						if (!target.mkdirs())
							throw new IOException("Couldn't create world directory!");
					String files[] = source.list();
					for (String file : files) {
						File srcFile = new File(source, file);
						File destFile = new File(target, file);
						copyFileStructure(srcFile, destFile);
					}
				} else {
					InputStream in = new FileInputStream(source);
					OutputStream out = new FileOutputStream(target);
					byte[] buffer = new byte[1024];
					int length;
					while ((length = in.read(buffer)) > 0)
						out.write(buffer, 0, length);
					in.close();
					out.close();
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
