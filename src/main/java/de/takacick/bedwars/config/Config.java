package de.takacick.bedwars.config;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.utils.Generator;
import de.takacick.bedwars.utils.GeneratorType;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Config {

	public void update() {
		File file = new File(BedWars.getInstance().getDataFolder() + "/" + "config" + ".yml");
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);

		config.set("team", null);
		BedWars.getInstance().getTeams().forEach(team -> {
			if (team.getSpawningPos() != null)
				config.set("team." + team.getName() + ".spawn", team.getSpawningPos());
			if (team.getBed() != null) {
				config.set("team." + team.getName() + ".bed", team.getBed());
				config.set("team." + team.getName() + ".bedface", team.getBedFace().name());
			}
			if (team.getSecretBed() != null) {
				config.set("team." + team.getName() + ".secretbed", team.getSecretBed());
				config.set("team." + team.getName() + ".secretbedface", team.getSecretFace().name());
			}
		});

		config.set("generator", null);
		List<Location> locations = new ArrayList<>();
		BedWars.getInstance().getGenerators().forEach(generator -> {
			locations.add(generator.getBlock());
		});
		config.set("generator", locations);

		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void load() {
		File file = new File(BedWars.getInstance().getDataFolder() + "/" + "config" + ".yml");
		if (!file.exists()) {
			return;
		}
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);

		if (config.isSet("team")) {
			BedWars.getInstance().getTeams().forEach(team -> {
				if (config.isSet("team." + team.getName() + ".spawn"))
					team.setSpawningPos((Location) config.get("team." + team.getName() + ".spawn"));
				if (config.isSet("team." + team.getName() + ".bed")) {
					team.setBed(((Location) config.get("team." + team.getName() + ".bed")).getBlock(),
							BlockFace.valueOf(config.getString("team." + team.getName() + ".bedface")), false);
				}
				if (config.isSet("team." + team.getName() + ".secretbed")) {
					team.setBed(((Location) config.get("team." + team.getName() + ".secretbed")).getBlock(),
							BlockFace.valueOf(config.getString("team." + team.getName() + ".secretbedface")), false);
				}
			});
		}

		if (config.isSet("generator")) {
			config.getList("generator").forEach(generator -> {
				Location location = (Location) generator;
				GeneratorType generatorType = BedWars.getInstance().getGeneratorType(location.getBlock().getType());
				if (generatorType != null) {
					BedWars.getInstance().getGenerators().add(new Generator(generatorType, location));
				}
			});
		}
	}

}
