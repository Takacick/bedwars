package de.takacick.bedwars.countdown;

import de.takacick.bedwars.BedWars;
import de.takacick.bedwars.utils.WinDetection;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class RestartCountdown extends Countdown {

	private boolean ignore;

	/**
	 * the countdown for the restart state
	 */
	public RestartCountdown(boolean ignore) {
		super("The server restarts in {count} seconds.", 121);
		this.addInterval(new int[] { 120, 60, 30, 20, 10, 5, 4, 3, 2, 1 });
		this.ignore = ignore;
	}

	@Override
	public void interrupt() {

		if (!ignore) {
			if (!WinDetection.check()) {
				BedWars.getInstance().setRestartCountdown(null);
				this.stop();
				return;
			}
		}

		// starting game
		if (this.getCount() == 0) {
			for (Player all : Bukkit.getOnlinePlayers()) {
				all.kickPlayer("§cserver is restarting..");
			}

			Bukkit.getWorlds().forEach(world -> {
				Bukkit.getServer().unloadWorld(world, false);
			});

			Bukkit.getScheduler().runTaskLater(BedWars.getInstance(), () -> {
				Bukkit.shutdown();
			}, 1l);
		}
	}
}