package de.takacick.bedwars.countdown;

import de.takacick.bedwars.BedWars;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

public abstract class Countdown {

	// instance variables
	private String message;
	private boolean started;
	private int countdown;
	private int count;
	private List<Integer> interval;

	/**
	 * constructor for objects of the class Countdown
	 * 
	 * @param message  the prefix of the countdown
	 * @param count the length of the countdown
	 */
	public Countdown(String message, int count) {
		this.message = message;
		this.started = false;
		this.count = count;
		this.interval = new ArrayList<Integer>();
	}

	/**
	 * edit this with countdown-specific methods and events
	 */
	public abstract void interrupt();

	/**
	 * when a broadcast should be send with the current time remaining
	 * 
	 * @param interval the seconds when message broadcasts corresponding to the
	 *                 countdown should be send
	 */
	public void addInterval(int[] interval) {
		for (int inter : interval) {
			this.interval.add(inter);
		}
	}

	/**
	 * starts the countdown
	 */
	public void start() {
		if (!isRunning()) {
			this.started = true;
			countdown = Bukkit.getScheduler().scheduleSyncRepeatingTask(BedWars.getInstance(), new Runnable() {
				@Override
				public void run() {
					count--;

					// broadcasting message if current count is in interval list
					if (interval.contains(count)) {
						Bukkit.broadcastMessage(message.replace("{count}", count + ""));
					}

					// executing countdown specific methods/events
					interrupt();
				}
			}, 20, 20);
		}
	}

	/**
	 * stops the countdown
	 */
	public void stop() {
		Bukkit.getScheduler().cancelTask(countdown);
		this.started = false;
	}

	/**
	 * @return if the countdown is running
	 */
	public boolean isRunning() {
		return this.started;
	}

	/**
	 * changes the count
	 * 
	 * @param count the new second the countdown should jump to
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * changes the beginning count and the current count
	 * 
	 * @param count the new length and count
	 */
	public void editCount(int count) {
		this.count = count;
	}

	/**
	 * @return current second
	 */
	public int getCount() {
		return this.count;
	}

	/**
	 * @return the prefix of the countdown (for displaying broadcasts)
	 */
	public String getMessage() {
		return this.message;
	}
}